
change-set "root-trusted-ca" {

  author  = "Simon Wenmouth <simon-wenmouth@users.noreply.github.com>"

  comment = "Configuration of a root certificate authority."

  // enabling file auditing

  audit-enable "file" {
    type = "file"
    options = {
      file_path = "/var/log/vault/vault_audit.log"
    }
  }

  // enabling and configuration of the pki backend

  mount "pki" {
    type = "pki"
    config {
      max_lease_ttl     = "87600h"
      default_lease_ttl = "87600h"
    }
  }

  write "pki/root/generate/internal" {
    data = {
      common_name = "${net.common_name}-r"
      ip_sans     = "${net.ip_sans}"
      alt_names   = "${net.dns_sans}"
      ttl         = "87600h"
    }
  }

  write "pki/roles/tlscert" {
    data = {
      max_ttl            = "8760h"
      allow_localhost    = true
      allow_any_name     = true
      allowed_domains    = "*.${net.domain}"
      organization       = "github.com/simon-wenmouth"
      ou                 = "test"
      no_store           = true
      allow_glob_domains = true
    }
  }

  // request certificate from pki backend (replacing the self signed certificate)

  write "pki/issue/tlscert" {
    data = {
      common_name = "${net.common_name}.${net.domain}"
      ttl = "8760h"
      ip_sans = "${net.ip_sans}"
      alt_names = "${net.dns_sans}"
    }
    output = {
      certificate = ">${pki.certificate}"
      issuing_ca = ">${pki.issuing_ca}"
      private_key = ">${pki.private_key}"
    }
  }

  // create policies

  policy-write "intermediate-ca-policy" {
    rules = <<EOF
      path "pki/intermediate/generate/exported" {
        capabilities = ["create","update"]
      }
      path "pki/root/sign-intermediate" {
        capabilities = ["create","update"]
      }
    EOF
  }

  policy-write "tlscert-policy" {
    rules = <<EOF
      path "pki/issue/tlscert" {
        capabilities = ["read", "create", "update"]
      }
    EOF
  }

  policy-write "vault-tool-role-secrets" {
    rules = <<EOF
      path "secret/vault-tool/roles/*" {
        capabilities = ["read"]
      }
    EOF
  }

  // enabling approle authentication

  auth-enable "approle" {
    type = "approle"
  }

  write "auth/approle/role/vault-tool" {
    data = {
      policies = [
        "vault-tool-role-secrets"
      ]
    }
  }

  write "auth/approle/role/consul" {
    data = {
      policies = [
        "tlscert-policy"
      ]
    }
  }

  write "auth/approle/role/vault" {
    data = {
      policies = [
        "tlscert-policy",
        "intermediate-ca-policy"
      ]
    }
  }

  // create and record the `role-id` and `secret-id` for the above `approle`s

  read "auth/approle/role/vault-tool/role-id" {
    output = {
      role_id = "secret/vault-tool/roles/vault-tool"
    }
  }

  write "auth/approle/role/vault-tool/secret-id" {
    output = {
      secret_id = "secret/vault-tool/roles/vault-tool"
    }
  }

  read "auth/approle/role/vault/role-id" {
    output = {
      role_id = "secret/vault-tool/roles/vault"
    }
  }

  write "auth/approle/role/vault/secret-id" {
    output = {
      secret_id = "secret/vault-tool/roles/vault"
    }
  }

  read "auth/approle/role/consul/role-id" {
    output = {
      role_id = "secret/vault-tool/roles/consul"
    }
  }

  write "auth/approle/role/consul/secret-id" {
    output = {
      secret_id = "secret/vault-tool/roles/consul"
    }
  }
}
