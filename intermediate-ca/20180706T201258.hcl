
change-set "intermediate-trusted-ca" {

  author  = "Simon Wenmouth <simon-wenmouth@users.noreply.github.com>"

  comment = "Configuration of a intermedicate certificate authority on Vault infrastructure."

  // enabling file auditing

  audit-enable "file" {
    type = "file"
    options = {
      file_path = "/var/log/vault/vault_audit.log"
    }
  }

  // enabling the pki backend

  mount "pki" {
    type = "pki"
    config {
      max_lease_ttl = "87600h"
      default_lease_ttl = "87600h"
    }
  }

  write "pki/intermediate/generate/internal" {
    data = {
      common_name = "${net.common_name}-i"
      alt_names = "${net.dns_sans}"
      ip_sans = "${net.ip_sans}"
    }
    output = {
      csr= ">${pki.csr}"
    }
  }

  write "pki/root/sign-intermediate" {
    data = {
      ttl = "78840h"
      csr = "<${pki.csr}"
      use_csr_values = "true"
      format = "pem_bundle"
    }
    output = {
      certificate = ">${pki.certificate}"
      issuing_ca = ">${pki.issuing_ca}"
    }
    client = {
      address = "${env.ROOT_CA_VAULT_ADDR}"
      token   = "${env.ROOT_CA_VAULT_TOKEN}"
      ca_cert = "${pki.issuing_ca}"
    }
  }

  write "pki/intermediate/set-signed" {
    data = {
      certificate = "<${pki.certificate}"
    }
  }

  write "pki/roles/tlscert" {
    data = {
      max_ttl = "8760h"
      allow_localhost = true
      allow_any_name = true
      allowed_domains = "*.${net.domain}"
      organization = "github.com/simon-wenmouth"
      ou = "test"
      no_store = true
      allow_glob_domains = true
    }
  }

  policy-write "tlscert-policy" {
    rules = <<EOF
      path "pki/issue/tlscert" {
        capabilities = ["read", "create", "update"]
      }
    EOF
  }

  policy-write "vault-tool-role-secrets" {
    rules = <<EOF
      path "secret/vault-tool/roles/*" {
        capabilities = ["read"]
      }
    EOF
  }

  auth-enable "approle" {
    type = "approle"
  }

  write "auth/approle/role/vault-tool" {
    data = {
      policies = ["vault-tool-role-secrets"]
    }
  }

  // create and record the `role-id` and `secret-id` for the above `approle`s

  read "auth/approle/role/vault-tool/role-id" {
    output = {
      role_id = "secret/vault-tool/roles/vault-tool"
    }
  }

  write "auth/approle/role/vault-tool/secret-id" {
    output = {
      secret_id = "secret/vault-tool/roles/vault-tool"
    }
  }

}
